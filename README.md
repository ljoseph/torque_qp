[![pipeline status](https://gitlab.inria.fr/auctus/panda/torque_qp/badges/master/pipeline.svg)](https://gitlab.inria.fr/auctus/panda/torque_qp-qp/commits/master)
[![Quality Gate](https://sonarqube.inria.fr/sonarqube/api/badges/gate?key=auctus:panda:torque_qp-qp)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:torque_qp-qp)
[![Coverage](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:torque_qp-qp&metric=coverage)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:torque_qp-qp)

[![Bugs](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:torque_qp-qp&metric=bugs)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:torque_qp-qp)
[![Vulnerabilities](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:torque_qp-qp&metric=vulnerabilities)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:torque_qp-qp)
[![Code smells](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:torque_qp-qp&metric=code_smells)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:torque_qp-qp)

[![Line of code](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:torque_qp-qp&metric=ncloc)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:torque_qp-qp)
[![Comment ratio](https://sonarqube.inria.fr/sonarqube/api/badges/measure?key=auctus:panda:torque_qp-qp&metric=comment_lines_density)](https://sonarqube.inria.fr/sonarqube/dashboard/index/auctus:panda:torque_qp-qp)

# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Atorque_qp-qp
- Documentation : https://auctus.gitlabpages.inria.fr/panda/torque_qp/index.html

# Torque QP

A generic low-level joint torque controller with a QP formulation. It's implemation has been done on the Franka Emika Panda Robot. The software includes a simulation part on gazebo and and integration with the franka_ros packages.

To learn more about this project, read the [wiki](https://gitlab.inria.fr/auctus/panda/torque_qp/-/wikis/home). 