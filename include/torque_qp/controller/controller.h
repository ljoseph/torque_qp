/*
 * Copyright 2020 <copyright holder> <email>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CONTROLLER_HPP
#define CONTROLLER_HPP

#pragma once

#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

// Ros specific
#include <controller_interface/multi_interface_controller.h>
#include <ros/node_handle.h>
#include <ros/package.h>
#include <ros/time.h>

// Eigen
#include <Eigen/Dense>

//Boost
#include <boost/scoped_ptr.hpp>

// qpOASES
#include <qpOASES.hpp>

// KDL 
#include <kdl/chain.hpp>
#include <kdl/chaindynparam.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainfksolvervel_recursive.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainjnttojacsolver.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/jacobian.hpp>
#include <kdl/jntspaceinertiamatrix.hpp>
#include <torque_qp/controller/chainjnttojacdotsolver.hpp>
#include <trac_ik/trac_ik.hpp>

// Conversion tool
#include <eigen_conversions/eigen_kdl.h>
#include <eigen_conversions/eigen_msg.h>
#include <kdl/utilities/error.h>
#include <kdl_conversions/kdl_msg.h>

// Messages
#include <realtime_tools/realtime_publisher.h>
#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/Path.h>
#include <panda_traj/PublishTraj.h>
#include <panda_traj/TrajProperties.h>
#include <torque_qp/PandaRunMsg.h>
#include <torque_qp/qpMsg.h>
#include <torque_qp/UI.h>

// TrajectoryGenerator
#include <panda_traj/panda_traj.hpp>


namespace Controller
{
class Controller
{
public:
    /**
    * \fn bool Init
    * \brief Initializes the controller
    * \param ros::NodeHandle& node_handle a ros node handle
    * \param Eigen::VectorXd q_init the robot initial joint position
    * \param Eigen::VectorXd qd_init the robot initial joint velocity
    * \return true if the controller is correctly initialized
    */
    bool init(ros::NodeHandle& node_handle, Eigen::VectorXd q_init, Eigen::VectorXd qd_init);

    /**
     * \fn Eigen::VectorXd update
     * \brief Update the controller to get the new desired joint velocity to send to the robot.
     * \param Eigen::VectorXd q the current joint position of the robot
     * \param Eigen::VectorXd qd the current joint velocity of the robot
     * \param const ros::Duration& period the refresh rate of the control
     * \return A Eigen::VectorXd with the desired joint velocity
     */
    Eigen::VectorXd update(Eigen::VectorXd q, Eigen::VectorXd qd, const ros::Duration& period);

private:
  template <class T>
  bool getRosParam(const std::string& param_name, T& param_data)
  {
    if (!ros::param::get(param_name, param_data))
    {
      ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
      ros::shutdown();
    }
    else
      ROS_INFO_STREAM(param_name << " : " << param_data);
    return true;
  }

  bool getRosParam(const std::string& param_name, Eigen::VectorXd& param_data)
  {
    std::vector<double> std_param;
    if (!ros::param::get(param_name, std_param))
    {
      ROS_FATAL_STREAM(" Problem loading parameters " << param_name << ". Check the Yaml config file. Killing ros");
      ros::shutdown();
    }
    else
    {
      if (std_param.size() == param_data.size())
      {
        for (int i = 0; i < param_data.size(); ++i)
          param_data(i) = std_param[i];
        ROS_INFO_STREAM(param_name << " : [" << param_data.transpose() << "]");
        return true;
      }
      else
      {
        ROS_FATAL_STREAM("Wrong matrix size for param " << param_name << ". Check the Yaml config file. Killing ros");
        ros::shutdown();
      }
    }
  }
  
    /**
    * \fn void init_publishers
    * \brief Initializes publishers
    * \param ros::NodeHandle& node_handle a ros node handle
    */
    void init_publishers(ros::NodeHandle& node_handle);

    /**
    * \fn void load_parameters
    * \brief Loads parameters from yaml file
    */
    void load_parameters();

    /**
    * \fn bool load_robot 
    * \brief Loads Panda robot
    * \param ros::NodeHandle& node_handle a ros node handle
    * \return true if the robot is correctly loaded
    */
    bool load_robot(ros::NodeHandle& node_handle);

    /**
    * \fn void do_publishing
    * \brief Publishes values
    * \return publish messages
    */
    void do_publishing();

    /**
    * \fn void BuildTrajectory
    * \brief Build the trajectory
    * \param KDL::Frame X_curr_ the current pose of the robot
    */
    void BuildTrajectory(KDL::Frame X_curr_);

    /**
    * @brief Publish the trajectory
    */
    void publishTrajectory();

    /**
    * @brief ros service to interact with the robot
    */
    bool updateUI(torque_qp::UI::Request& req, torque_qp::UI::Response& resp);
    
    /**
    * @brief ros service to update the trajectory
    */
    bool updateTrajectory(panda_traj::UpdateTrajectory::Request &req, panda_traj::UpdateTrajectory::Response &resp);
  
     // Publishers
    geometry_msgs::Pose X_curr_msg_, X_traj_msg_;
    geometry_msgs::Twist X_err_msg_;
    realtime_tools::RealtimePublisher<geometry_msgs::PoseArray> pose_array_publisher;
    realtime_tools::RealtimePublisher<nav_msgs::Path> path_publisher;
    realtime_tools::RealtimePublisher<torque_qp::PandaRunMsg> panda_rundata_publisher;
    realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped> pose_curr_publisher, pose_des_publisher;

    // Services
    ros::ServiceServer  updateUI_service,
                        updateTraj_service;

    /**
     * @brief Solver for the inverse kinematic
    **/
    boost::shared_ptr<TRAC_IK::TRAC_IK> ik_solver;

    /**
     * @brief solver to compute the Jacobian
    **/
    boost::scoped_ptr<KDL::ChainJntToJacSolver> chainjacsolver_;

    /**
     * @brief solver to compute the Jdot_qdot
    **/
    boost::scoped_ptr<KDL::ChainJntToJacDotSolver> chainjnttojacdotsolver_;

    /**
     * @brief The dynamic solver.
     */
    boost::scoped_ptr<KDL::ChainDynParam> dynModelSolver_;

    /**
     * @brief The forward kinematic solver for position
     */
    boost::scoped_ptr<KDL::ChainFkSolverPos_recursive> fksolver_;

    /**
     * @brief The forward kinematic solver for velocity
     */
    boost::scoped_ptr<KDL::ChainFkSolverVel_recursive> fksolvervel_;

    KDL::Chain chain; /*!< @brief KDL chain corresponding to the robot */
    KDL::JntArray ll; /*!< @brief Joint lower limits vector*/  
    KDL::JntArray ul; /*!< @brief Joint upper limits vector*/
    KDL::JntArray coriolis_kdl; /*!< @brief Coriolis torque vector*/
    KDL::JntArray gravity_kdl; /*!< @brief gravity torque vector*/
    KDL::Twist Jdqd_kdl; /*!< @brief Jdotqdot vector*/
    KDL::Jacobian J_; /*!< @brief KDL Jacobian Matrix*/
    KDL::JntSpaceInertiaMatrix M_; /*!< @brief KDL inertia matrix in joint space */
    KDL::Frame X_curr_; /*!< @brief KDL current Cartesian pose of the tip_link */
    KDL::Frame X_traj_; /*!< @brief KDL desired Cartesian pose of the tip_link */
    KDL::FrameVel Xd_curr_; /*!< @brief KDL current twist of the tip_link */
    KDL::Twist Xd_traj_; /*!< @brief KDL desired twist of the tip_link */
    KDL::Twist Xdd_traj_; /*!< @brief KDL current twist acceleration of the tip_link */
    KDL::Twist X_err_;  /*!< @brief KDL desired Cartesian error between the desired and current pose */
    KDL::JntArrayVel q_in; /*!< @brief KDL joint position of the robot */

    Eigen::VectorXd p_gains_; /*!< @brief Proportional gains of the PID controller */ 
    Eigen::VectorXd i_gains_; /*!< @brief Derivative gains of the PID controller */
    Eigen::VectorXd d_gains_; /*!< @brief Integral gains of the PID controller */

    Eigen::VectorXd torque_max_; /*!< @brief Joint torque limits */ 
    Eigen::VectorXd joint_torque_out_; /*!< @brief Results of the QP optimization */

    Eigen::Matrix<double, 6, 1> xdd_des_; /*!< @brief Desired robot acceleration of the robot tip_link */
    Eigen::Matrix<double, 6, 1> xd_curr_; /*!< @brief Eigen current twist of the tip_link */
    Eigen::Matrix<double, 6, 1> x_curr_;  /*!< @brief Eigen current Cartesian pose of the tip_link */
    Eigen::Matrix<double, 6, 1> xdd_traj; /*!< @brief Eigen current twist acceleration of the tip_link */
 
    double regularisation_weight_; /*!< @brief Regularisation weight */
    int dof; /*!< @brief Number of degrees of freedom of the robot */

    std::string root_link_; /*!< @brief base link of the KDL chain */
    std::string tip_link_; /*!< @brief tip link of the KDL chain (usually the end effector*/

    Eigen::Matrix<double, 6, 1> x_err; /*!< @brief Eigen desired Cartesian error between the desired and current pose */
    Eigen::Matrix<double, 6, 1> jdot_qdot_; /*!< @brief Eigen jdotqdot vector*/
    Eigen::MatrixXd gravity; /*!< @brief Eigen gravity vector*/
    Eigen::MatrixXd coriolis; /*!< @brief Eigen coriolis vector*/
    Eigen::MatrixXd J; /*!< @brief Eigen Jacobian Matrix*/
    Eigen::MatrixXd M; /*!< @brief Eigen inertia matrix in joint space */


    // Matrices for qpOASES
    // NOTE: We need RowMajor (see qpoases doc)
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> H_; /*!< @brief Hessian matrix of the QP*/
    Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> A_; /*!< @brief Constraint matrix of the QP */

    Eigen::VectorXd g_; /*!< @brief Gradient vector of the QP */
    Eigen::VectorXd lb_; /*!< @brief Lower bound vector of the QP */
    Eigen::VectorXd ub_; /*!< @brief Upper bound vector of the QP */
    Eigen::VectorXd lbA_; /*!< @brief Constraint lower bound vector of the QP */
    Eigen::VectorXd ubA_; /*!< @brief Constraint upper bound vector of the QP */
    Eigen::VectorXd qd_min_; /*!< @brief Minimum joint velocity limit vector*/
    Eigen::VectorXd qd_max_; /*!< @brief Maximum joint velocity limit vector*/

    Eigen::VectorXd nonLinearTerms_; /*!< @brief Matrix representing M^{-1} ( g(q) + c(q,qd) ) */
    std::unique_ptr<qpOASES::SQProblem> qpoases_solver_; /*!< @brief QP solver point*/
    int number_of_constraints_; /*!< @brief Number of constraints of the QP problem*/
    int number_of_variables; /*!< @brief Number of optimization variables of the QP problem*/
    Eigen::Matrix<double, 6, Eigen::Dynamic> a_; /*!< @brief Matrix to ease comprehension */
    Eigen::Matrix<double, 6, 1> b_; /*!< @brief Matrix to ease comprehension */

    // Trajectory variables
    TrajectoryGenerator trajectory; /*!< @brief TrajectoryGenerator object */
    panda_traj::TrajProperties traj_properties_; /*!< @brief Properties of the trajectory */

    bool sim;  /*!< @brief Get if code is on the real robot to remove the gravity */
    Eigen::VectorXd k_reg; /* Proportional gain for the damping in the regularisation term */
};
}  // namespace Controller
#endif  // CONTROLLER_HPP
