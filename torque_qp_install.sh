sudo apt install python-rosdep python-catkin-tools python-wstool python-vcstool build-essential cmake git libpoco-dev libeigen3-dev ros-melodic-combined-robot-hw	

# libfranka
mkdir -p ~/franka_ros_ws
git clone --recursive https://github.com/frankaemika/libfranka ~/franka_ros_ws/libfranka
cd ~/franka_ros_ws/libfranka
git checkout 0.7.1
git submodule update
mkdir build; cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .

# franka ros
cd ~/franka_ros_ws
git clone --recursive https://github.com/frankaemika/franka_ros src/franka_ros
cd src/franka_ros
git checkout melodic-devel
cd ~/franka_ros_ws
catkin config --init --extend /opt/ros/melodic --cmake-args -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS=-std=c++11 -DFranka_DIR:PATH=~/franka_ros_ws/libfranka/build
sudo apt install ros-melodic-combined-robot-hw
catkin build
source ~/franka_ros_ws/devel/setup.bash

# torque_qp.

mkdir -p ~/auctus_ws/src
cd ~/auctus_ws
catkin config --init --extend ~/franka_ros_ws/devel --cmake-args -DCMAKE_BUILD_TYPE=Release -DFranka_DIR:PATH=~/franka_ros_ws/libfranka/build -DCMAKE_CXX_FLAGS=-std=c++11


cd ~/auctus_ws/src
git clone https://gitlab.inria.fr/auctus/panda/torque_qp.git
wstool init 
wstool merge torque_qp/torque_qp.rosinstall
wstool update
cd ..
rosdep install --from-paths src --ignore-src -r -y
catkin build

# Setup environment
echo 'source ~/auctus_ws/devel/setup.bash' >> ~/.bashrc
echo 'export GAZEBO_RESOURCE_PATH=/usr/share/gazebo-9:~/auctus_ws/src/franka_description/worlds' >> ~/.bashrc
echo 'export GAZEBO_MODEL_PATH=~/auctus_ws/src/franka_description/robots' >> ~/.bashrc
